package fr.uavignon.ceri.tp2;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import fr.uavignon.ceri.tp2.data.Book;
import fr.uavignon.ceri.tp2.data.BookRepository;

public class ListViewModel extends AndroidViewModel {

    BookRepository bookRepository;
    private LiveData<List<Book>> allBooks;

    public ListViewModel (@NonNull Application application) {
        super(application);
        bookRepository = new BookRepository(application);
        allBooks = bookRepository.getAllBooks();
    }

    public LiveData<List<Book>> getAllBooks()
    {
        return allBooks;
    }
    public void insertProduct(Book book) { bookRepository.insertBook(book); }
    public void findProduct(long id) {
        bookRepository.findBook(id);
    }
    public void deleteProduct(long id) { bookRepository.deleteBook(id); }
}
